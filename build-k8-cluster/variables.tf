variable "project_name" {}
variable "vpc_cidr_block" {}
variable "subnet_cidr_block" {
  type = map(any)
}
variable "avail_zone" {}
variable "env_prefix" {}
variable "instance_type" {}
variable "public_key_location" {}
variable "my_pvt_key" {}
variable "ami_id" {}
variable "my_ip" {}
variable "worker-node-count" {}