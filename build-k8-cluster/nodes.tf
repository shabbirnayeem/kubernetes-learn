# creating AWS key pair from scratch using your PUB key
resource "aws_key_pair" "ssh-key" {
  key_name = "server-key"
  # using file() to read the pub key file
  public_key = file(var.public_key_location)
}


# create ec2 instances
resource "aws_instance" "master-node" {
  ami           = var.ami_id
  instance_type = var.instance_type

  subnet_id              = values(aws_subnet.subnets)[0].id
  vpc_security_group_ids = [aws_security_group.control-plane-SG.id]
  availability_zone      = var.avail_zone

  associate_public_ip_address = true

  # using the new public key
  key_name = aws_key_pair.ssh-key.key_name

  # install docker and nginx from bash script
  # user_data = file("master.sh")

  tags = {
    "Name" = "${var.project_name}-master-node"
    "Type" = "Control-Node"
  }
}

resource "aws_instance" "worker-node" {
  count         = var.worker-node-count
  ami           = var.ami_id
  instance_type = var.instance_type

  subnet_id              = values(aws_subnet.subnets)[0].id
  vpc_security_group_ids = [aws_security_group.worker-node-SG.id]
  availability_zone      = var.avail_zone

  associate_public_ip_address = true

  # using the new public key
  key_name = aws_key_pair.ssh-key.key_name

  # install docker and nginx from bash script
  # user_data = file("worker.sh")

  tags = {
    "Name" = "${var.project_name}-woker-node${format("%d", count.index + 1)}"
    "Type" = "Worker-Node"
  }
}

# adding the dns to the master-nodes /etc/hosts file
resource "null_resource" "master_setup" {

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file(var.my_pvt_key)
    host        = aws_instance.master-node.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "echo ${aws_instance.master-node.private_ip} master | sudo tee -a /etc/hosts",
      "echo ${element(aws_instance.worker-node.*.private_ip, 0)} worker1 | sudo tee -a /etc/hosts",
      "echo ${element(aws_instance.worker-node.*.private_ip, 1)} worker2 | sudo tee -a /etc/hosts",
    ]

  }
}

# adding the dns to the worker-nodes /etc/hosts file

resource "null_resource" "worker_setup" {
  count = var.worker-node-count

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file(var.my_pvt_key)
    host        = element(aws_instance.worker-node.*.public_ip, count.index)
  }

  provisioner "remote-exec" {
    inline = [
      "sudo hostnamectl set-hostname worker${format("%d", count.index + 1)}",
      "echo ${aws_instance.master-node.private_ip} master | sudo tee -a /etc/hosts",
      "echo ${element(aws_instance.worker-node.*.private_ip, 0)} worker1 | sudo tee -a /etc/hosts",
      "echo ${element(aws_instance.worker-node.*.private_ip, 1)} worker2 | sudo tee -a /etc/hosts"
    ]

  }
}

resource "null_resource" "configure-server-worker" {
  provisioner "local-exec" {
    command = "ansible-playbook -i inventory_aws_ec2.yaml configure-k8-cluster.yaml"

  }
  depends_on = [aws_instance.worker-node, aws_instance.master-node]
}