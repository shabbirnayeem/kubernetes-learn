project_name        = "k8-cluster"
ami_id              = "ami-053b0d53c279acc90"
avail_zone          = "us-east-1a"
instance_type       = "t2.medium"
public_key_location = "~/.ssh/id_ed25519.pub"
env_prefix          = "dev"
vpc_cidr_block      = "172.32.0.0/16"
worker-node-count   = 2
subnet_cidr_block = {
  "subnet1" = {
    az         = "us-east-1a"
    cidr_block = "172.32.16.0/20"
  }
  "subnet2" = {
    az         = "us-east-1b",
    cidr_block = "172.32.32.0/20"
  }
}
