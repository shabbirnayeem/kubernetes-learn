# resource "aws_lb" "k8-test-lb" {
#   name               = "ingress-nginx-lb"
#   internal           = false
#   load_balancer_type = "application"
#   subnets            = [for subnet in aws_subnet.subnets : subnet.id]
#   security_groups    = [aws_security_group.k8-lb-sg.id]
# }

# resource "aws_lb_target_group" "node-target-group" {
#   name     = "ingress-nginx-tg"
#   port     = 30687
#   protocol = "HTTP"
#   vpc_id   = aws_vpc.k8-cluster.id
# }

# resource "aws_lb_target_group_attachment" "attach" {
#   count = length(aws_instance.worker-node)

#   target_group_arn = aws_lb_target_group.node-target-group.arn
#   target_id        = aws_instance.worker-node[count.index].id
#   port             = 30687
# }

# resource "aws_lb_listener" "lister" {
#   load_balancer_arn = aws_lb.k8-test-lb.arn
#   port              = 80
#   default_action {
#     target_group_arn = aws_lb_target_group.node-target-group.arn
#     type             = "forward"
#   }

# }