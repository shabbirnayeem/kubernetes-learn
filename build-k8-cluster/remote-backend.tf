terraform {
  backend "s3" {
    bucket = "sn-labs-devops"
    key    = "kubernetes-learn/build-k8-cluster/k8-cluster.tfstate"
    region = "us-east-1"
  }
}
