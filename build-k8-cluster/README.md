# Kubernetes Learn: Build Kubernetes Cluster

This repository provides a guide and resources for building a Kubernetes (K8s) cluster. The `build-k8-cluster` folder contains detailed instructions and necessary files to set up a K8s cluster from scratch in AWS.


## Caution
Please note, using this project will incur costs in AWS.

## Prerequisites

Before starting, ensure you have the following installed:

- AWS Account
- [Install terraform](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)
- [Install Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)
- [Install AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)


## Installation

1. **Clone the repository**:
    ```bash
    git clone https://gitlab.com/shabbirnayeem/kubernetes-learn.git
    cd kubernetes-learn/build-k8-cluster
    ```

2. **Build Kubernetes infrastructure and configurations**:
    ```bash
    terraform apply
    ```

## Project Structure

- `configure-k8-cluster.yaml`: Ansible configuration file to configure nodes.
- `terraform files`: Build Kubernetes infrastructure.
- `master.sh and worker.sh`: Install kubelet packages.
- `k8-deployment-files`: contains example's of different types of k8 service deployment.

## Usage

After applying the configurations, you can check the status of your pods and services:

```bash
kubectl get pods
kubectl get services
```

## Troubleshooting
```bash
kubectl logs <pod-name>
```

## Credit
This project is part of "The Ultimate Kubernetes Administrator Course" by Techworld with Nana. It serves as a comprehensive guide to learning Kubernetes.
For more information about the course, [check out the course link](https://www.techworld-with-nana.com/kubernetes-administrator-cka).