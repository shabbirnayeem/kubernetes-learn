output "master-node-ip" {
  value = aws_instance.master-node.private_ip
}

output "worker-node-ip" {
  value = [for node in aws_instance.worker-node : node.private_ip]

}
