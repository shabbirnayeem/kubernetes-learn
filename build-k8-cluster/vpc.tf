data "aws_region" "current" {}

resource "aws_vpc" "k8-cluster" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_hostnames = true
  tags = {
    "Name" = "${var.project_name}-${var.env_prefix}-vpc"
  }
}

resource "aws_subnet" "subnets" {
  for_each          = var.subnet_cidr_block
  vpc_id            = aws_vpc.k8-cluster.id
  cidr_block        = each.value["cidr_block"]
  availability_zone = each.value["az"]

  tags = {
    "Name" = "${var.project_name}-${var.env_prefix}-${each.key}"
  }

}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.k8-cluster.id

  tags = {
    "Name" = "${var.project_name}-igw"
  }

}


# Create a route table
resource "aws_route_table" "route-table" {
  vpc_id = aws_vpc.k8-cluster.id

  # add entry into the route table
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
  tags = {
    "Name" = "${var.project_name}-rtb"
  }
}

# Subnet Association with Route Table
resource "aws_route_table_association" "rtb-subnet-association" {
  for_each       = var.subnet_cidr_block
  subnet_id      = aws_subnet.subnets[each.key].id
  route_table_id = aws_route_table.route-table.id
}



