# Configure Secuirty Gorup rules
resource "aws_security_group" "control-plane-SG" {
  name   = "control-plane-SG"
  vpc_id = aws_vpc.k8-cluster.id

  # define inbound rules: Allow SSH
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }

  # Kubernetes API server
  ingress {
    from_port   = 6443
    to_port     = 6443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # etcd server client API
  ingress {
    from_port   = 2379
    to_port     = 2380
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  # Kubelet API
  ingress {
    from_port   = 10250
    to_port     = 10250
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  # kube-scheduler
  ingress {
    from_port   = 10259
    to_port     = 10259
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  # kube-controller-manager
  ingress {
    from_port   = 10257
    to_port     = 10257
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  # weave net
  ingress {
    from_port   = 6783
    to_port     = 6783
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  # # weave net
  # ingress {
  #   from_port   = 6783
  #   to_port     = 6788
  #   protocol    = "udp"
  #   cidr_blocks = [var.vpc_cidr_block]
  # }

  # define outbound rules: Allow all traffic to go out
  egress {
    from_port = 0
    to_port   = 0
    # allow all protocol
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

  tags = {
    "Name" = "control-plane-sg"
  }

}


resource "aws_security_group" "worker-node-SG" {
  name   = "worker-node-SG"
  vpc_id = aws_vpc.k8-cluster.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.my_ip]
  }

  # Kubelet API
  ingress {
    from_port   = 10250
    to_port     = 10250
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  # NodePort Services†
  ingress {
    from_port   = 30000
    to_port     = 32767
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # weave net
  ingress {
    from_port   = 6783
    to_port     = 6783
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr_block]
  }

  # # weave net
  # ingress {
  #   from_port   = 6783
  #   to_port     = 6788
  #   protocol    = "udp"
  #   cidr_blocks = [var.vpc_cidr_block]
  # }

  # define outbound rules: Allow all traffic to go out
  egress {
    from_port = 0
    to_port   = 0
    # allow all protocol
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }
  tags = {
    "Name" = "worker-node-sg"
  }
}

resource "aws_security_group" "k8-lb-sg" {
  name   = "k8-lb-SG"
  vpc_id = aws_vpc.k8-cluster.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # define outbound rules: Allow all traffic to go out
  egress {
    from_port = 0
    to_port   = 0
    # allow all protocol
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
    prefix_list_ids = []
  }

}