# Microservices in Kubernetes

### Introduction to Microservices
- Microservices are an architectural approach to software development
- Software is composed of small independent services (instead of having a huge monolith)
- Each business functionality is encapsulated into own Microservice

### Benefits
- Each MS can be developed, packaged, and released independently
- Changes in 1 MS doesn’t effect other MS
- Less interconnected logic, loosely coupled
- Each MS can be developed by separate developer teams

## DevOps Tasks
### Deploy Microservices App
- Developers develop the microservice applications
- As a DevOps engineer your task would be to Deploy the existing microservices application in a K8s cluster

### Information needed from Developer:
- Which services you need to deploy?
- Which service is talking to which service?
- How are they communicating?
- Which database are they using? 3rd party services
- On which port does each service run?

### Prepare k8s Environment
- Deploy any 3rd part apps
- create Secrets and ConfigMaps for microservices

### Create Deployment and Services for each microservices
- Example:
    ```
    ---
    apiVersion: apps/v1
    kind: Deployment
    metadata:
    name: emailservice
    spec:
    replicas: 2
    selector:
        matchLabels:
        app: emailservice
    template:
        metadata:
        labels:
            app: emailservice
        spec:
        containers:
        - name: server
            image: gcr.io/google-samples/microservices-demo/emailservice:v0.2.3
            ports:
            - containerPort: 8080
            env:
            - name: PORT
            value: "8080"
            readinessProbe:
            periodSeconds: 5
            exec:
                command: ["/bin/grpc_health_probe", "-addr=:8080"]
            livenessProbe:
            periodSeconds: 10
            exec:
                command: ["/bin/grpc_health_probe", "-addr=:8080"]
            resources:
            requests:
                cpu: 100m
                memory: 64Mi
            limits:
                cpu: 200m
                memory: 128Mi
    ---
    apiVersion: v1
    kind: Service
    metadata:
    name: emailservice
    spec:
    type: ClusterIP
    selector:
        app: emailservice
    ports:
    - protocol: TCP
        port: 5000
        targetPort: 8080
    ```

# Create Helm Chart for Microservices

## Basic Structure of Helm Chart

### Create a shared Helm Chart
    helm create heml-microservices

### Create Microservices Helm Chart
- Values Object
- A built-in object
- By default values is empty
- Values are passed into template, from 3 sources:
    - the values.yaml file in the chart
    - the user-supplied file passed with -f flag
    - parameter passed with —set flag
- Variable Naming Conventions
    - Names should begin with a lowercase letter
    - Separated with camelcase
- Flat or Nested Values
    - Values may be flat or nested deeply
        - Flat
            ```
            # deployment.yaml
            metadata:
            name: {{ .Values.appName }}
            spec:
            replicas: {{ .Values.appReplicas }}

            # values.yaml
            appName: test-app
            appReplicas: 2
            ```
        - Nested
            ```
            # deployment.yaml
            metadata:
            name: {{ .Values.app.name }}
            spec:
            replicas: {{ .Values.app.rplicas }}

            # values.yaml
            app:
                name: test-app
                replicas: 2
            ```
        - Best practice is to use flat structure
- Example:
    ```
    # deplyment.yaml
    apiVersion: apps/v1
    kind: Deployment
    metadata:
    name: {{ .Values.appName }}
    spec:
    replicas: {{ .Values.appReplicas }}
    selector:
        matchLabels:
        app: {{ .Values.appName }}
    template:
        metadata:
        labels:
            app: {{ .Values.appName }}
        spec:
        containers:
        - name: {{ .Values.appName }}
            image: "{{ .Values.appImage }}:{{ Values.appVersion }}"
            ports:
            - containerPort: {{ .Values.containerPort }}


    # service.yaml

    apiVersion: v1
    kind: Service
    metadata:
    name: {{ .Values.appName }}
    spec:
    type: {{ .Values.serviceType }}
    selector:
        app: {{ .Values.appName }}
    ports:
    - protocol: TCP
        port: {{ .Values.servicePort }}
        targetPort: {{ .Values.containerPort }}
    ```
- Dynamic Environment Variables in templet file
    - Single  Environment Variables
        ```
        env:
        - name: {{ .Values.containerEnvVar.key }}
          value: {{ .Values.containerEnvVar.value }}
        ```
    - Multiple  Environment Variables
        ```
        env:
        # range to loop trough all the vairables passed in from the values.yaml file
        # with this users can provide more than one variable
        {{-range .Values.containerEnvVar}}
        - name: {{ .key }}
        # using quote to add "" around the values value
        value: {{ .value | quote }}
        {{-end}}
        ```
### Configure Values file
- Note each deployment requires its own values.yaml file
    ```
    appName: servicename
    appImage: gcr.io/google-samples/microservices-demo/servicename
    appVersion: v.0.0
    appReplicas: 2
    containerPort: 8080
    containerEnvVars:
    - name: ENV_VAR_ONE
    value: "valueone"
    - name: ENV_VAR_TWO
    value: "valuetwo"

    servicePort: 8080
    serviceType: ClusterIP
    ```

### helm commands
- `helm template`: render chart templates locally and display the output
    - syntax: ` helm template -f {values_file_name.yaml} {chart_name} `
- `helm lint`: examines a chart for possible issues
    - syntax: `helm lint-f {values_file_name.yaml} {chart_name} `
- `helm install —dry-run`: check generate manifest without installing the chart

### Deploy using Helmfile
- What is a Helm file?
    - Helmfile is a declarative specification for deploying Helm charts that adds functionality to Helm
- Example
    ```
    releases:
  - name: rediscart
    chart: chart/redis
    values:
      - helm-microservices-values/redis-service-values.yaml
    ```
#### Install the releases using helmfile command
##### Make sure to install helmfile tool
- For Ubuntu:
    - Download the Helmfile from [Github](https://github.com/roboll/helmfile/releases)
        ```
        mv helmfile_linux_amd64 helmfile
        chmod 777 helmfile
        sudo mv hrlmfile /usr/local/bin
        ```
    - source: https://jhooq.com/helmfile-manage-helmchart/

##### Install
- Run the following command where the helmfile.yaml is located:
    `hemlfile sync `
- To destroy, run the following command:
    `helm destroy`
